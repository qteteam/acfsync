# ACFSync
Syncs acf fields in acf-json. Plugin should be used as a MU-plugin to make sure that the sync works.

## Why?
Use this plugin to keep acf-fields version-controlled and synced between dev and prod enviroments