<?php

/*
Plugin Name: ACFSync
Description: Sync ACF Fields as json to keep them versioned
Version: 1.0
Author: rahlenjakob
*/


add_filter('acf/settings/save_json', function ($path) {
    return dirname(__FILE__) . '/acf-json';
});
add_filter('acf/settings/load_json', function ($paths) {
    unset($paths[0]);
    $paths[] = dirname(__FILE__) . '/acf-json';
    return $paths;
});